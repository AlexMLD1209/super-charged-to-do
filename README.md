# Super Charged TODO App

## Tech stack

- node v20.13.1
  - npm (this is installed with node)
- JavaScript
- [React.js](https://react.dev/)
  - [create-react-app](https://create-react-app.dev/)
- [Tailwind CSS](https://tailwindcss.com/)
- [Express.js](https://expressjs.com/)
- [SQLite](https://www.sqlite.org/)
- [Jest](https://jestjs.io/) (unit testing)
- [sequelize](https://sequelize.org/) (ORM)

## Table of Contents

- [Step 1 - React state setup](#step-1---react-state-setup)
- [Step 2 - Persisting state to localStorage / sessionStorage](#step-2---persisting-state-to-localstorage--sessionstorage)
- [Step 3 - Our first API](#step-3---our-first-api)
- [Step 4 - FE + BE](#step-4---fe--be)
- [Step 5 - More of a Real Database](#step-5---more-of-a-real-database)
- [Step 6 - Is our data safe](#step-6---is-our-data-safe)
- [Step 7 - One more entity](#step-7---one-more-entity)
- [Step 8 - Where are our users?](#step-8---where-are-our-users)
- [Step 9 - Let's add the assignee](#step-9---lets-add-the-assignee)
- [Step 10 - Better looking users!](#step-10---better-looking-users)
- [Step 11 - UI Refinements!](#step-11---ui-refinements)
- [Step 12 - Is our code stable?](#step-12---is-our-code-stable)
- [Step 13 - BE Authentication](#step-13---be-authentication)
- [Step 14 - FE Authentication](#step-14---fe-authentication)
- [Step 15 - Authentication Clean-Ups](#step-15---authentication-clean-ups)
- [Step 16 - Logout](#step-16---logout)
- [Step 17 - Time to add some API Documentation](#step-17---time-to-add-some-api-documentation)

## Step 1 - React state setup

The UI has already been provided using Tailwind CSS.
Currently the project is displaying under the `Dashboard` page 7 hardcoded tasks. Your job is bring the app to life by using React `useState` hook for state management.

Instead of displaying the hardcoded tasks, we will provide functionality to the `Create Task` and `Delete Task` / `Edit Task` (under `EditTaskModal`) to either add or update our tasks state (`useState`)

Additionally, we will implement [React router](https://reactrouter.com/en/main) so we can see all our current available pages

### Acceptance criteria

1. We will manage the tasks state using the `useState` hook
2. As a user, I can add a new task by clicking on the `Create Task` button in the dashboard, after which I should be able to see the task listed in the `Dashboard` in the appropriate column
3. I can edit an existing task using the `EditTaskModal` (opens by clicking the task). After saving, the new changes should be present in the task displaying in the `Dashboard`
4. I can delete an existing task. Once done, the task should no longer display in the `Dashboard`
5. Implement React router

## Step 2 - Persisting state to localStorage / sessionStorage

We want to be able to see the tasks even if the user refreshes the browser. We will use `localStorage` or `sessionStorage` for this purposes (you decide which one)

### Acceptance criteria

1. All added tasks should be persisted into `localStorage`/`sessionStorage` as well, so when the user refreshes the browser, the data is kept and displayed on the `Dashboard` again (our state manager `useState` should be initialize on first render with any info stored in `localStorage`/`sessionStorage`)

## Step 3 - Our first API

We will create a small API using Express.js: https://expressjs.com/en/starter/generator.html
You will be provided with a project with the following folder structure:

```
super-charged-be/
├── src/
│   ├── database/
│   │   └── tasks.json
│   ├── middlewares/
│   │   └── errorHandler.ts
│   ├── tasks/
│   │   └── task.controller.ts
│   │   └── task.enums.ts
│   │   └── task.interface.ts
│   │   └── task.routes.ts
│   │   └── task.service.ts
│   ├── utils/
│   │   └── statusCodes.ts
│   ├── server.ts
├── .env
├── .prettierrc
├── package-lock.json
├── package.json
├── tsconfig.json
```

Important!: Take a look at how the project is structured and the files that are already created to LEARN how to create an API with Express.js.
The database folder contains a JSON file with some data.
You will need to read this file and create the routes to perform the CRUD operations.

### Acceptance criteria

1. For the following tasks, you need to add implementation for `task.controller.ts` and `task.service.ts`:
   1. Create a GET route to get all tasks
   2. Create a POST route to create a task
   3. Create a PUT route to update a task by id
   4. Create a DELETE route to delete a task by id
2. All operations must be performed against the database/tasks.json file using the node fs module

## Step 4 - FE + BE

At this moment we already a have a small interface created which stores some data in the browser `localStorage`. Besides that, we already have an API which can store task information.
So it’s time to wire up our first application!

Few notes:

https://www.dhiwise.com/post/react-environment-variables-your-guide-to-secure-react-apps

Please make sure you use env variables to connect to the service **(we will need this for production deployment)**

### Acceptance criteria

1. Remove the local storage from FE and connect to the API we just created
2. Create environment variable for API service URL named `REACT_APP_API_URL`, it will store the URL of our API
3. All the actions that made changes into local storage should now be connected to the API and work properly

## Step 5 - More of a Real Database

Now we want to use a real database. We could use MongoDB, or SQL but since the use case it’s pretty simple we are going to use [SQLite](https://www.sqlite.org/docs.html)

Few notes:

https://www.youtube.com/watch?v=m2kpl7LuSW0, https://dev.to/mayank30/express-rest-api-simple-crud-using-sqlite-i94

Check the structure from `Step 3`, you should have a database folder to handle the connection, and models separately

### Acceptance criteria

1. Add model for the tasks : “Task” (find the right structure)
2. Use sequelize to interact with the model
3. Update our API so that it now performs actions against the Database

## Step 6 - Is our data safe ?

Now that we have a model and a database in our API, we should make sure whenever we create a new entry that our data is valid, and since no one relies on FE (FE should not be trusted :) )
BE has the responsibility to do this.

Few notes:

https://www.geeksforgeeks.org/how-to-implement-validation-in-express-js/

### Acceptance criteria

1. Add validation for the specific fields we expect from FE
2. Serialize the entity, do not take into consideration extra properties on the request

## Step 7 - One more entity

In each application there are users, and since our project handles tasks, each task should have an assignee. But our system so far does not know more information about such an entity. In this step, we want to extend our API in order to manage one more model: “User”.

### Acceptance criteria

1. Create a new “Users” model, which for now should be a pretty basic structure: firstName, lastName, age, email.
2. Create CRUD operations under a new route “**/users”** for the new model
3. Add relationship between the Task and User
4. Add validation for the API
5. Get endpoint should expect parameters: search, pagination

## Step 8 - Where are our users?

Since we have one more entity now, we should be able to manage this entity in our application

Clicking on the `Users` tab (on the sidenav) should request and display all the currently existing users in our Database

### Acceptance criteria

1. Clicking on the `Dashboard` tab should request and display the user's tasks
2. Clickin on the `Users` tab should request and display existing users
3. I want to be able to add new users using a button above the users table
4. I want to be able to edit and delete users I want to be able to add new users using a button above the users table
5. I want to be able to edit and delete users

## Step 9 - Let's add the assignee

Now that we have a new entity, and we manage it on our FE, we should have an assignee on each task. In order to do this, we should add some extra logic to our application.

Our GET endpoint already is designed in such a way that it expects a search parameter, so we are going to extend the Task creation with a search input, and we are going to implement our first **HOOK ! .**

React Custom Hooks: https://legacy.reactjs.org/docs/hooks-custom.html

We are going to implement a hook called useDebounceCallback, which is going to make sure that we do not aggressive trigger requests while we use the search input, so we should trigger requests in more manner way after the user finishes typing.

Time for some reusability: the same input should be used for the Task editing, and also we want to add one more place, when we click the username to open a modal with the same input in order to change only the assignee.

### Acceptance criteria

1. As a user, I want to be able to add an assignee to a task.
2. As a user, I want to be able to search for the users and assign the correct one
3. As a user, I want to be able to change just the assignee by checking the assignee name.

## Step 10 - Better looking users!

Now that we fully manage the users and each task has an assignee, we should be thinking at making everything look better. For now, we display the name of the user on the Task, but this is not an elegant approach.
So since we want to give personality to users and also make our Tasks look better, we are going to add an Avatar image to each user, this will require certain changes on both BE and FE.
On BE side, we have to extend our model to hold an image URL, and we have to save the file on our service avatars/images (name it how you consider it's best)
One more thing we have to do on our service is to make a public route which will allow us to fetch the content inside the specific folder you save the images.

Also add validation for the image, we should restrict the size to an avatar image.

Notes: https://www.npmjs.com/package/multer

Now that we are done with BE side, time to get to FE, we have now when we create or edit a user we have to put a file uploader, and choose an image from our system as an avatar. Then now it's time to display them, when we are on edit mode on the user we should display the avatar, also the table should be updated to display the image, and the task should replace the name of the user with the avatar ( clicking functionality is kept)

**Optional**: make the search show the avatar and the name

### Acceptance criteria

1. As a user, I want to be able to add an avatar to the user
2. As a user, I want to be able to edit the avatar
3. As a user, I want to be able to see the avatar in the table and edit mode
4. As a user, I want to see the user avatar instead of the name in the Task

## Step 11 - UI Refinements!

Remember we have a status on our ticket now it's time to revamp a bit the UI in order to make everything look better. At this step, we are going to create three big columns which will display the tasks based on their Priority. (do not forget about reusability at this step)

At this step, we are going to extend a bit the Task with one more property: "Priority”.
Possible values for Priority: "Low”, "Medium”, "High”.

We're going to extend the Task model with this new property and add validation for it to allow only the values mentioned above. On the FE side, we are going to add a tag with a corresponding color: (gray = low, yellow=medium, red=high), and by default the columns will be sorted by priority(high-to-low).

### Acceptance criteria

1. As a user, I want to see my Tasks split based on status
2. As a user, I want to have a priority system for the tasks
3. As a user, I want to be able to manage the priority for the tasks
4. As a user, I want to see the priorities on each Task, and have them filtered accordingly

## Step 12 - Is our code stable?

At this step, we are going to add some check-ups in order to make sure the code we just wrote is stable and well verified.
There are two stages which can help us assure we have a clean, well-running codebase: linting and testing.
More than that, we do not want to perform this steps manually, so we are going to use a pre-commit hook: https://www.npmjs.com/package/husky which will run this stages before each commit we have, if something fails, you won't be able to commit.
Also on this step we are going to add unit tests for all our components, and for all our routes on the Express service.

Notes: https://testing-library.com/docs/react-testing-library/intro/ , https://medium.com/@biteship/unit-test-integration-test-in-express-js-194b93391f79

### Techincal Requirements

1. Have husky pre-commit to trigger linting and tests
2. Have 70%+ unit tests coverage on both FE and BE
3. Optional: add command to husky to not allow commiting if coverage is below 70%

From now on, all the code we add should be covered by tests

# Authentication Intro

Now that we have a good looking application based on a solid codebase. It's time to make it a "real” one. We need to add authentication system.

We will split them into few steps, handling authentication on BE side, then we need to implement a register page, login page forwarded by few changes on the users table, and to add the logged in user on the platform on the left side corner.

## Step 13 - BE Authentication

Here we are going to implemeent the authentication for the Express service in order to do that we need to install few more libraries: jsonwebtoken, bcryptjs.

### Acceptance criteria

1. Extend the user Model with a password
2. Make sure the passowrd is hashed and salted when stored to SQLite
3. Create an implement user registration (CREATE) extend already existing route for user creation or add a new one.
4. Create a login route, which expects credentials as a payload: email and password
5. Inside the login handler define a SECRET_KEY. In case the user is not found based on the email, return 401, if the user is found and password is compared correctly, generate a token and assign it to cookies. If this actions performed correctly return 201.
6. Create an authentication middleware which will be placed before login. Inside this handler check if we have the token which we assigned to the cookies on login and the decoded token matches the provided user in the body, if we have it go to next in the middleware chain, if not return 403 unnouthrized.

Voila, our service is protected !

## Step 14 - FE Authentication

Here first we need to implement the login and register page, and connect them with our newly created routes, so that we can create new users and be able to login them into our system.

Once you have the register page ready, check the SQLite db to check if you create the users correctly.

When login page is fully implemented it's time to get to implementing some protected routes. At this moment we find our first use case for a context. A context is a global state management utility. A context is like a state variable, but it can be used all over the platform. For example in our case we will add a state inside the context hook: isAuthenticated which will be set to true once the login requests is succesfull, also alongside this we will store the returned user from the request.

After we have the context setup we will create a Provider which will wrap the whole application, once we have it wrapped any component can access the context hook and retrieve the information stored inside.

Next step is to have 2 types of Route components, basic Route( the ones we used before) and a ProtectedRoute, this new type of Route will check the context and if isAuthenticated is false it will automatically redirect to login, if not will render the component.

### Acceptance criteria

1. As a user, I want to be able to register on the platform
2. As a user, I want to be able to login into the platform.

## Step 15 - Authentication Clean-Ups

Since now we create users on the register page, we need to make few changes on the users table, there should be no option to create a user from the table, and also all the actions should be disabled for now, instead we are going to add on the top right side an user avatar, and once click you can edit your user information.

Optional: automatic prompt to the user to add an avatar at first login

### Acceptance criteria

1. As a user, I don't want to be able to edit other users information, just mine.

## Step 16 - Logout

We want once we are logged in to be able to also log out.

In order to do that we need a route into our express server which will remove the token from the request.

On FE side we have to add a button which once triggered will call the endpoint we just created at the step above. If the endpoint returns 200, we set isAuthenticated to false, and automatically all the protected routes become unavailable until next login

### Acceptance criteria

1. As an user, I want to be able to logout from the system

## Step 17 - Time to add some API Documentation

At this step we are going to add https://swagger.io/ . Let’s assume we are working on a big team and we don’t want everyone to constantly ask us how we do this, how we call this, how that model looks, here swagger comes in by offering a UI documentation based on our API. This also helps QA to test better the APIs we build.
https://blog.logrocket.com/documenting-express-js-api-swagger/

### Acceptance criteria

1. Add swagger support for all exiting routes
