import { apiHelper } from "./apiHelper";

export const getTasks = async () => {
  return await apiHelper.get("/tasks");
};

export const createTask = async (newTask) => {
  return await apiHelper.post("/tasks", newTask);
};

export const editTask = async (taskId, taskData) => {
  return await apiHelper.put(`/tasks/${taskId}`, taskData);
};

export const removeTask = async (taskId) => {
  return await apiHelper.delete(`/tasks/${taskId}`);
};
