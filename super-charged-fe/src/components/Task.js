import { TASK_STATUS_COLOR } from "../enums";
import EditTaskModal from "./EditTaskModal";

function Task({ task, updateTask, deleteTask }) {
  return (
    <>
      <EditTaskModal
        task={task}
        modalId={`modal${task.id}`}
        updateTask={updateTask}
        deleteTask={deleteTask}
      />
      <div
        className={`p-4 bg-${
          TASK_STATUS_COLOR[task.status]
        }-200 rounded cursor-pointer`}
        data-modal-target={`modal${task.id}`}
        data-modal-toggle={`modal${task.id}`}
      >
        <h2
          className={`text-xl font-bold text-${
            TASK_STATUS_COLOR[task.status]
          }-800 dark:text-${TASK_STATUS_COLOR[task.status]}-800 mb-2`}
        >
          {task.title}
        </h2>
        <p
          className={`text-${TASK_STATUS_COLOR[task.status]}-700 dark:text-${
            TASK_STATUS_COLOR[task.status]
          }-600`}
        >
          {task.description}
        </p>
      </div>
    </>
  );
}

export default Task;
