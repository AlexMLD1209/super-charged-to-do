import Task from "./Task";
import { TASK_STATUS_COLOR } from "../enums";

const TASK_STATUS_TITLE = {
  TODO: "To Do",
  IN_PROGRESS: "In Progress",
  COMPLETED: "Completed!",
};

function TaskList({ status, tasks, updateTask, deleteTask }) {
  return (
    <div
      className={`bg-${TASK_STATUS_COLOR[status]}-100 p-4 rounded-lg shadow-lg`}
    >
      <h2
        className={`text-2xl font-bold mb-4 text-${TASK_STATUS_COLOR[status]}-800`}
      >
        {TASK_STATUS_TITLE[status]}
      </h2>
      <div className="space-y-2">
        {tasks.map((task) => (
          <Task task={task} updateTask={updateTask} deleteTask={deleteTask} />
        ))}
      </div>
    </div>
  );
}

export default TaskList;
