import Layout from "../components/Layout";

function Users() {
  return (
    <Layout>
      <div className="relative overflow-x-auto">
        <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3">
                First Name
              </th>
              <th scope="col" className="px-6 py-3">
                Last Name
              </th>
              <th scope="col" className="px-6 py-3">
                Email
              </th>
              <th scope="col" className="px-6 py-3">
                Age
              </th>
            </tr>
          </thead>
          <tbody>
            <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
              <th
                scope="row"
                className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
              >
                Steve
              </th>
              <td className="px-6 py-4">Jobs</td>
              <td className="px-6 py-4">steve.jobs@apple.com</td>
              <td className="px-6 py-4">55</td>
            </tr>
            <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
              <th
                scope="row"
                className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
              >
                Bill
              </th>
              <td className="px-6 py-4">Gates</td>
              <td className="px-6 py-4">bill.gates@microsoft.com</td>
              <td className="px-6 py-4">62</td>
            </tr>
            <tr className="bg-white dark:bg-gray-800">
              <th
                scope="row"
                className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
              >
                Warren
              </th>
              <td className="px-6 py-4">Buffett</td>
              <td className="px-6 py-4">warren.buffett@berkshire.com</td>
              <td className="px-6 py-4">90</td>
            </tr>
          </tbody>
        </table>
      </div>
    </Layout>
  );
}

export default Users;
