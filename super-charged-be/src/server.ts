import dotenv from 'dotenv';
import express from 'express';
import morgan from 'morgan';
import taskRoutes from '@tasks/task.routes';
import cors from 'cors';
import { errorHandler } from '@middlewares/errorHandler';

dotenv.config();

// Create Express server
const app = express();

// Enable CORS
app.use(cors());

// Use the express.json() middleware to parse JSON request bodies
app.use(express.json());

// Use the morgan middleware to log HTTP requests
app.use(morgan('dev'));

app.use('/api/tasks', taskRoutes);

app.use(errorHandler);

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
