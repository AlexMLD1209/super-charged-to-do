import fs from 'fs/promises';
import path from 'path';
import { Task } from './task.interfaces';

// Construct the file path to the tasks.json file. __dirname represents the current directory of the executing script.
// This line joins the current directory (__dirname) with the relative path "../database/tasks.json",
// resulting in the absolute path to the tasks.json file on the filesystem.
const filePath = path.join(__dirname, '../database/tasks.json');

const readFile = async (): Promise<Task[]> => {
  const data = await fs.readFile(filePath, 'utf-8');
  return JSON.parse(data);
};

const writeFile = async (taskList: Task[]) => {
  await fs.writeFile(filePath, JSON.stringify(taskList, null, 2), 'utf-8');
};

export const getAllTasks = async (): Promise<Task[]> => {
  return await readFile();
};

export const createTask = async (task: Task): Promise<Task> => {
  const tasks = await readFile();
  const newTask = { id: tasks.length + 1, ...task };
  tasks.push(newTask);
  await writeFile(tasks);
  return newTask;
};

export const updateTask = async (
  id: number,
  updatedTask: Task,
): Promise<Task | null> => {
  const tasks = await readFile();
  const index = tasks.findIndex((task) => task.id === id);
  if (index === -1) return null;
  tasks[index] = { ...tasks[index], ...updatedTask };
  await writeFile(tasks);
  return tasks[index];
};

export const deleteTask = async (id: number) => {
  const tasks = await readFile();
  const index = tasks.findIndex((task) => task.id === id);
  if (index === -1) return null;
  const [deletedTask] = tasks.splice(index, 1);
  await writeFile(tasks);
  return deletedTask;
};
