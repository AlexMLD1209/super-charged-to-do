import { Request, Response } from 'express';
import * as tasksService from './task.service';
import * as statusCodes from '@utils/statusCodes';

export const getTasks = async (req: Request, res: Response) => {
  try {
    const tasks = await tasksService.getAllTasks();
    res.status(statusCodes.OK).json(tasks);
  } catch (error) {
    res
      .status(statusCodes.INTERNAL_SERVER_ERROR)
      .json({ message: 'Internal Server Error' });
  }
};

export const createTask = async (req: Request, res: Response) => {
  try {
    const { title, description, status } = req.body;

    if (!title || !description || !status) {
      return res
        .status(statusCodes.BAD_REQUEST)
        .json({ message: 'Title, description and status are required' });
    }
    const newTasks = await tasksService.createTask({
      title,
      description,
      status,
    });
    res.status(statusCodes.CREATED).json(newTasks);
  } catch (error) {
    res
      .status(statusCodes.INTERNAL_SERVER_ERROR)
      .json({ message: 'Internal Server Error' });
  }
};

export const updateTask = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const { title, description, status } = req.body;
    if (!title || !description || !status) {
      return res.status(statusCodes.BAD_REQUEST).json({
        message: 'Title, description, and status are required',
      });
    }
    const updatedTask = await tasksService.updateTask(Number(id), {
      title,
      description,
      status,
    });
    if (!updatedTask) {
      return res
        .status(statusCodes.NOT_FOUND)
        .json({ message: 'Task not found' });
    }
    res.status(statusCodes.OK).json(updatedTask);
  } catch (error) {
    res
      .status(statusCodes.INTERNAL_SERVER_ERROR)
      .json({ message: 'Internal Server Error' });
  }
};

export const deleteTask = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const deleteTask = await tasksService.deleteTask(Number(id));
    if (!deleteTask) {
      return res
        .status(statusCodes.NOT_FOUND)
        .json({ message: 'Task not found' });
    }
    res.status(statusCodes.OK).json({ message: 'Task deleted successfully' });
  } catch (error) {
    res
      .status(statusCodes.INTERNAL_SERVER_ERROR)
      .json({ message: 'Internal Server Error' });
  }
};
